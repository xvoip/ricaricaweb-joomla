<?php
/**
 * Telecash Ricaricaweb Module
 *
 * @package    Acticode.Telecash.Ricaricaweb
 * @subpackage Modules
 * @license    GNU/GPL, see LICENSE.php
 * @link       http://www.telecash.it/ricaricaweb/plugins/joomla
 * mod_ricaricaweb is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

defined('_JEXEC') or die();

class JFormRuleCouponDefault extends JFormRule
{

	public function test(SimpleXMLElement $element, $value, $group = null, JRegistry $input = null, JForm $form = null)
	{
		$data = JRequest::getVar('jform', array(), 'post', 'array');
//		$sdata = JFactory::getApplication()->input;
	        if ($data["params"]["tc_coupon_enable_default"]=="1"&&$value=="") {
//	        if ($data->get("tc_coupon_enable_default")=="1"&&$value=="") {
			$element->attributes()->message = "Please, specify the coupon code or disable the 'Set predefined coupon' option.";
			return false;
	        } else
			return true;
	}

}
