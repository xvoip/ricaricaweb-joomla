<?php
/**
 * Telecash Ricaricaweb Module
 *
 * @package    Acticode.Telecash.Ricaricaweb
 * @subpackage Modules
 * @license    GNU/GPL, see LICENSE.php
 * @link       http://www.telecash.it/ricaricaweb/plugins/joomla
 * mod_ricaricaweb is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

defined('_JEXEC') or die;
require_once dirname(__FILE__) . '/helper.php';

$config = array(
	"tc_merchant" => $params->get('tc_merchant'),
	"tc_alias" => $params->get('tc_alias'),
	"tc_lng" => $params->get('tc_lng'),
	"tc_url" => $params->get('tc_url'),
	"tc_debug" => $params->get('tc_debug'),
	"tc_country" => $params->get('tc_country'),
	"tc_taglio" => $params->get('tc_taglio'),
	"tc_usetcjs" => $params->get('tc_usetcjs'),
	"tc_set_phone_credit" => $params->get('tc_set_phone_credit'),
	"tc_set_order_item" => $params->get('tc_set_order_item'),
	"tc_set_order_detail" => $params->get('tc_set_order_detail'),
	"tc_require_customer_email" => $params->get('tc_require_customer_email'),
	"tc_page_background_color" => $params->get('tc_page_background_color'),
	"tc_disable_paypal" => $params->get('tc_disable_paypal'),
	"tc_doublenum" => $params->get('tc_doublenum'),
	"tc_affiliate_merchant" => $params->get('tc_affiliate_merchant'),
	"tc_coupon_enable" => $params->get('tc_coupon_enable'),
	"tc_coupon_enable_default" => $params->get('tc_coupon_enable_default'),
	"tc_coupon_default" => $params->get('tc_coupon_default'),
	"tc_coupon_default_hidden" => $params->get('tc_coupon_default_hidden'),
	"tc_form_layout" => $params->get('tc_form_layout')
);

$document = JFactory::getDocument();
$document->addStyleSheet('modules/mod_ricaricaweb/css/frontend.css');

$form = modRicaricawebHelper::getRicaricaweb($config,$params);
$matchs = preg_match("/tbox\[([a-zA-Z0-9\-]+)\]/", $form, $templates);
require JModuleHelper::getLayoutPath('mod_ricaricaweb', $templates[1]);


