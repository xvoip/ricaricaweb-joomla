<?php 
/**
 * @package     Telecash.Ricaricaweb
 * @subpackage  mod_ricaricaweb
 *
 * @copyright   Copyright (C) 2005 - 2017 Gabriele Valenti, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>
<div class="tc_div_ricaricaweb">
<div class="tc_div_content">
<?php echo $form; ?>
</div>
</div>
