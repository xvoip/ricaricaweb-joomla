<?php
/**
 * Helper class for Telecash Ricaricaweb Module
 *
 * @package    Acticode.Telecash.Ricaricaweb
 * @subpackage Modules
 * @link http://www.telecash.it/ricaricaweb/plugin/joomla
 * @license        GNU/GPL, see LICENSE.php
 * mod_helloworld is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

defined('_JEXEC') or die();

require_once( __DIR__ . '/classes/ricaricaweb.php' );

class ModRicaricawebHelper
{
    /**
     * Retrieves the payment form
     *
     * @param   array  $params An object containing the module parameters
     *
     * @access public
     */
    public static function getRicaricaweb($config,$params="")
    {
	$rw = new ricaricaweb();
	// languages
	$lngs = array("it","en","es","fr","de");
	$flds = array(
		"tc_string_yourphonenumber",
		"tc_string_yourphonenumber2",
		"tc_string_youremail",
		"tc_string_pay",
		"tc_string_cancel",
		"tc_string_paymentok",
		"tc_string_paymenterr",
		"tc_string_insertcoupon"
	);
	$rw->messages["translations"]=array();
	foreach ($lngs as $lng) {
		foreach ($flds as $fld)
			$rw->messages["translations"][$lng][$fld] = $params->get($fld."_".$lng);
	}
	$keys = array("ITA"=>"it","ENG"=>"en","FRA"=>"fr","ESP"=>"es","DEU"=>"de");
	$rw->currLang = $keys[$params->get("tc_lng")];
	return $rw->manageResponse($config);
    }

}
