<?php
/**
 * Telecash Ricaricaweb Module
 *
 * @package    Acticode.Telecash.Ricaricaweb
 * @subpackage Modules
 * @license    GNU/GPL, see LICENSE.php
 * @link       http://www.telecash.it/ricaricaweb/plugins/joomla
 * mod_ricaricaweb is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

defined('_JEXEC') or die();

class tc_db {

	var $db;

	public function __construct() {
		$this->db = JFactory::getDbo();
	}

	public function query($q) {
		$this->db->setQuery($q);
		return $this->db->Execute($q);
	}

	public function select($q) {
		$this->db->setQuery($q);
		return $this->db->loadObjectList($q);
	}

}
