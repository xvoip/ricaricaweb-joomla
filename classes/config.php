<?php
/**
 * Telecash Ricaricaweb Module
 *
 * @package    Acticode.Telecash.Ricaricaweb
 * @subpackage Modules
 * @license    GNU/GPL, see LICENSE.php
 * @link       http://www.telecash.it/ricaricaweb/plugins/joomla
 * mod_ricaricaweb is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */

defined('_JEXEC') or die();

class tc_config {

	public function getConf() {

		$module = JModuleHelper::getModule('mod_ricaricaweb');
		$params = new JRegistry($module->params);
		$this->merchant = $params->tc_merchant;
		$this->lang = $params->tc_lang;
		$this->country = $params->tc_country;
		$this->useTcJs = $params->tc_usetcjs;
		$this->tagli = $params->tc_tagli;
		$this->alias = $params->tc_alias;
		$this->merchant_email = $params->tc_merchant_email;
		$this->order_detail = $params->tc_set_order_detail;
		$this->order_item = $params->tc_set_order_item;
		$this->debug = $params->tc_debug;
		$this->tcUrl = $params->tc_URL;
		$this->tc_doublenum = $params->tc_doublenum;
		$this->tc_affiliate_merchant = $params->tc_affiliate_merchant;
		$this->tc_coupon_enable = $params->tc_coupon_enable;
		$this->tc_coupon_enable_default = $params->tc_coupon_enable_default;
		$this->tc_coupon_default = $params->tc_coupon_default;
		$this->tc_coupon_default_hidden = $params->tc_coupon_default_hidden;
		$this->tc_form_layout = $params->tc_form_layout;
		return $this;

	}


}


