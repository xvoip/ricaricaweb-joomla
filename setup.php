<?php
/**
 * @package     Telecash.Ricaricaweb
 * @subpackage  mod_ricaricaweb
 *
 * @copyright   Copyright (C) 2005 - 2017 Gabriele Valenti, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
 
class mod_ricaricawebInstallerScript
{

	function install($parent) 
	{
		echo '<p>The module has been installed</p>';
	}
 
	function uninstall($parent) 
	{
		echo '<p>The module has been uninstalled</p>';
	}
 
	function update($parent) 
	{
		echo '<p>The module has been updated to version' . $parent->get('manifest')->version . '</p>';
	}
 
	function preflight($type, $parent) 
	{
		echo '<p>Anything here happens before the installation/update/uninstallation of the module</p>';
	}
 
	function postflight($type, $parent) 
	{
		echo '<p>Anything here happens after the installation/update/uninstallation of the module</p>';
	}
}
